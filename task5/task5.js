$(function()	{
    $('td').click(function(e)	{

        let t = e.target;

        let elmName = t.tagName.toLowerCase();

        if(elmName === 'input')	{return false;}

        let val = $(this).html();
        let code = '<input type="text" id="edit" value="'+val+'" />';
        $(this).empty().append(code);
        $('#edit').focus();
        $('#edit').blur(function()	{
            let val = $(this).val();
            $(this).parent().empty().html(val);
        });
    });
});

$(document).keydown(function(event){

    if(event.keyCode === 13) {
        $('#edit').blur();
    }
});