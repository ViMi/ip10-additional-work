$(function(){
	$.fn.task4Filter = function () {

		let filterTarget = $(this);
		let child;
		if ($(this).is('ul')) {
			child = 'li';
		} else if ($(this).is('ol')) {
			child = 'li';
		} else if ($(this).is('table')) {
			child = 'tbody tr';
		}

		let hide;
		let show;
		let filter;


		$('input.filter').keyup(function() {

			filter = $(this).val();

			hide = $(filterTarget).find(child + ':not(:Contains("' + filter + '"))');
			show = $(filterTarget).find(child + ':Contains("' + filter + '")');
			
			hide.hide();
			show.show();

		});

	}

});