$(function(e) {
    e.fn.AddXbutton = function(options) {
        let defaults = {
            img: 'images/task3.png'
        };
        let opts = e.extend(defaults, options);
        e(this)
            .after(e('<input type="image" id="xButton" src="' + opts['img'] + '" />')
                .css({
                    'display': 'none',
                    'cursor': 'pointer',
                    'margin-left': '-15px',
                    'width':'10px',
                    'height':'10px',
                })
                .click(function() {
                    e('#Input').val('').focus();
                    e('#xButton').hide();
                }))
            .keyup(function() {
                if (e(this).val().length > 0) {
                    e('#xButton').show();
                } else {
                    e('#xButton').hide();
                }
            });
        if (e(this).val() !== '') e('#xButton').show();
    };
});

$(document).ready(function() {
    $('#Input').AddXbutton({
        img: 'images/task3.png'
    });
    $('#Input').focus();
});